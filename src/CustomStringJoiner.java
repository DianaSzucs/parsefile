public class CustomStringJoiner {
    private String string ="";
    private String delimiter;

    public CustomStringJoiner(String delimiter) {
        this.delimiter = delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public void addToken(String s) {
        string += s + delimiter;
    }

    public String removeToken(String s) {
        string = string.replace(s+delimiter,"");
        return string;
    }

    public String getString() {
        return string;
    }
}
