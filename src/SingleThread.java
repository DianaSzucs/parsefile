import java.io.File;
import java.io.IOException;

public class SingleThread implements Runnable{
    private File folder;

    public SingleThread(File folder) {
        this.folder = folder;
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();
        try {
            Main.parseAllFiles(folder);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        long elapsedTime = System.nanoTime() - startTime;
        System.out.println("The single thread reads all files in : " + elapsedTime/1000000 + " miliseconds");
    }
}
